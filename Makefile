MODULE_NAME := IdealGas

MAJOR := 0
MINOR := 0
PATCH := 0


OBJS := sound_speed.o


TEST_OBJS := sound_speed_tests.o \
             ideal_gas_tests.o


LIBS :=
paCages := HydroBase


include ./Makefile.paCage/Makefile
