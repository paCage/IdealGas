#ifndef _IDEAL_GAS_H_
#define _IDEAL_GAS_H_


#ifdef __cplusplus
extern "C" {
#endif

/* Universal gas constant */
#define ig_cnst_R (8.314) /* J / mol K */

/* Specific heat capacity at constant volume for mon/di/polyatomic ideal gas*/
#define ig_cv (3.0 / 2.0 * ig_cnst_R)
#define ig_cv_mon (3.0 / 2.0 * ig_cnst_R)
#define ig_cv_di (5.0 / 2.0 * ig_cnst_R)
#define ig_cv_poly (3.0 * ig_cnst_R)

/* Specific heat capacity at constant pressure for mon/di/polyatomic ideal gas*/
#define ig_cp (5.0 / 2.0 * ig_cnst_R)
#define ig_cp_mon (5.0 / 2.0 * ig_cnst_R)
#define ig_cp_di (7.0 / 2.0 * ig_cnst_R)
#define ig_cp_poly (4.0 * ig_cnst_R)

/* Ratio of specific heat capacities, gamma */
#define gamma (ig_cp_mon / ig_cv_mon)
#define gamma_mon (gamma)
#define gamma_di (ig_cp_di / ig_cv_di)
#define gamma_poly (ig_cp_poly / ig_cv_poly)

/* Combinations of gamma */
#define g_m_1 (gamma - 1.0)
#define g_p_1 (gamma + 1.0)
#define g_m_1__g_p_1 (g_m_1 / g_p_1)
#define gamma_inv (1.0 / gamma)
#define g_m_1__2_g (g_m_1 / (2.0 * gamma))
#define g_p_1__2_g (g_p_1 / (2.0 * gamma))

/* Hydro vars floors */
#define rho_min (1.e-10)
#define cs_min (1.e-10)
#define p__rho_min (cs_min * cs_min / gamma)
#define p_min (rho_min * p__rho_min)
#define e_min (cs_min * cs_min / (gamma * (gamma - 1.0)))
#define HYDRO_T_LEN (5)


#ifdef __cplusplus
}
#endif


#endif /* _IDEAL_GAS_H_ */
