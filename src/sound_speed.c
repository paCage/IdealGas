/*
 * sound_speed.c
 * tests_file: sound_speed_tests.c
 *
 * Calculating the sound speed of a given hydro struct based on
 * c_s = sqrt(gamma * p / rho)
 *
 * @param: h: A pointer to a given hydro state
 *
 * @return: Calculated sound speed in the same units as hydro vars (cs_min
 * in the case that calculated sound speed is smaller than cs_min)
 */


#include "./ideal_gas.h"
 #include "./sound_speed.h"


 double sound_speed(hydro_t *h)
 {
   double cs = gamma * h->p / h->rho;

   return (cs > cs_min) ? cs : cs_min;
 }
