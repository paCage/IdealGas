#ifndef _SOUND_SPEED_H_
#define _SOUND_SPEED_H_


#include <HydroBase.h>


#ifdef __cplusplus
extern "C" {
#endif

double sound_speed(hydro_t *);

#ifdef __cplusplus
}
#endif


#endif /* _SOUND_SPEED_H_ */
