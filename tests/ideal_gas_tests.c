/*
 * ideal_gas_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/ideal_gas.h"


Describe(ideal_gas);
BeforeEach(ideal_gas) {};
AfterEach(ideal_gas) {};


Ensure(ideal_gas, has_correct_definitions)
{
  assert_that_double(ig_cnst_R, is_equal_to_double(8.314));

  assert_that_double(ig_cv, is_equal_to_double(3.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cv_mon, is_equal_to_double(3.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cv_di, is_equal_to_double(5.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cv_poly, is_equal_to_double(3.0 * ig_cnst_R));

  assert_that_double(ig_cp, is_equal_to_double(5.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cp_mon, is_equal_to_double(5.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cp_di, is_equal_to_double(7.0 / 2.0 * ig_cnst_R));
  assert_that_double(ig_cp_poly, is_equal_to_double(4.0 * ig_cnst_R));

  assert_that_double(gamma, is_equal_to_double(5.0 /3.0));
  assert_that_double(gamma_mon, is_equal_to_double(5.0 /3.0));
  assert_that_double(gamma_di, is_equal_to_double(7.0 / 5.0));
  assert_that_double(gamma_poly, is_equal_to_double(4.0 / 3.0));

  assert_that_double(g_m_1, is_equal_to_double(gamma - 1));
  assert_that_double(g_p_1, is_equal_to_double(gamma + 1));
  assert_that_double(g_m_1__g_p_1, is_equal_to_double((gamma - 1) / (gamma + 1)));
  assert_that_double(gamma_inv, is_equal_to_double(1.0 / gamma));
  assert_that_double(g_m_1__2_g, is_equal_to_double((gamma - 1.0) / (2.0 * gamma)));
  assert_that_double(g_p_1__2_g, is_equal_to_double((gamma + 1.0) / (2.0 * gamma)));

  assert_that_double(rho_min, is_equal_to_double(1.e-10));
  assert_that_double(cs_min, is_equal_to_double(1.e-10));
  assert_that_double(p__rho_min, is_equal_to_double(1.e-20 / gamma));
  assert_that_double(p_min, is_equal_to_double(1.e-30 / gamma));
  assert_that_double(e_min, is_equal_to_double(1.e-20 / (gamma * (gamma - 1))));
}
