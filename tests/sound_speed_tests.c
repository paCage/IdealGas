/*
 * sound_speed_tests.c
 */


#include <cgreen/cgreen.h>
#include "./../src/ideal_gas.h"
#include "./../src/sound_speed.h"

#define RHO 1.23 /* kg m^-3 */
#define P 2.34 /* N m^-2 */

hydro_t h;


Describe(sound_speed);

BeforeEach(sound_speed)
{
  h.rho = RHO;
  h.p = P;
};

AfterEach(sound_speed) {};


Ensure(sound_speed, calculates_sound_speed_based_on_a_given_hydro_struct)
{
  double cs = sound_speed(&h);
  assert_that_double(cs, is_equal_to_double(gamma * P / RHO));
}


Ensure(sound_speed, returns_the_cs_floor_to_avoid_negative_or_zero_pressure)
{
  h.p = 0.0;
  double cs = sound_speed(&h);
  assert_that_double(cs, is_equal_to_double(cs_min));
}
